const express = require('express');
const app = express();
const JiraClient = require('./src/clients/JiraClient');
const jira = new JiraClient();
const config = require('./src/config');

app.use(express.json());

app.use((req, res, next) => {
    console.log(`${req.method}: ${req.originalUrl}`);
    res.append('Access-Control-Allow-Origin', '*');
    next();
});

app.options('*', (req, res) => {
    res.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE');
    res.append('Access-Control-Allow-Headers', '*');
    res.status(204).send();
});

app.all('/get-my-open-issues-list', async (req, res, next) => {
    try {
        const {data} = await jira.getMyOpenIssuesList();

        res.json(data);
    } catch (error) {
        next(error);
    }
});

app.all('/report-worklog/:issueKey', async (req, res, next) => {
    try {
        const issueKey = req.params.issueKey;
        const timeSpentSeconds = req.body.timeSpentSeconds;
        const startDate = req.body.startDate;
        const comment = req.body.comment;

        console.log({issueKey, timeSpentSeconds, startDate, comment});

        const {data} = await jira.reportWorklog(issueKey, timeSpentSeconds, startDate, comment);

        res.json(data);
    } catch (error) {
        next(error);
    }
});

app.post('/find-issue', async (req, res, next) => {
    try {
        const jql = req.body.jql;

        console.log(jql);

        const {data} = await jira.findIssue(jql);

        res.json(data);
    } catch (error) {
        next(error);
    }
});

app.post('/issue', async (req, res, next) => {
    try {
        const body = req.body;

        console.log({body});

        const {data} = await jira.createIssue(body);

        res.json(data);
    } catch (error) {
        next(error);
    }
});

app.get('/active-sprint/:boardId', async (req, res, next) => {
    try {
        const boardId = req.params.boardId;

        const {data} = await jira.getActiveSprint(boardId);

        res.json(data);
    } catch (error) {
        next(error);
    }
});

app.use((err, req, res, next) => {
    console.error(`ERROR: ${err}`);

    if (res.headersSent) {
        return next(err);
    }

    res.status(500);
    res.json({error: ''+err});
});

app.listen(config.jiraProxy.port, () => console.log(`CORS listening on port ${config.jiraProxy.port}.`));