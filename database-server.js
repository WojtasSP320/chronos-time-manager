const express = require('express');
const app = express();
const config = require('./src/config');
const tasksRepository = require('./src/repository/TasksRepository');
const worklogsRepository = require('./src/repository/WorklogsRepository');
const pool = require('./src/repository/ConnectionPool');

app.use(express.json());

app.use((req, res, next) => {
    console.log(`${req.method}: ${req.originalUrl}`);
    res.append('Access-Control-Allow-Origin', '*');
    next();
});

app.options('*', (req, res) => {
    res.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE');
    res.append('Access-Control-Allow-Headers', '*');
    res.status(204).send();
});

app.get('/task/:key', async (req, res, next) => {
    const key = req.params.key;

    try {
        const results = await tasksRepository.findTaskByKey(key);

        res.json(results.length === 1 ? results[0] : null);
    } catch (error) {
        next(error);
    }
});

app.get('/tasks/by-type/:type', async (req, res, next) => {
    const type = req.params.type;

    try {
        const results = await tasksRepository.getTasksByType(type);

        res.json(results);
    } catch (error) {
        next(error);
    }
});


app.post('/task', async (req, res, next) => {
    const key = req.body.key;
    const summary  = req.body.summary;

    try {
        await tasksRepository.createTask(key, summary);
        res.status(201).send();
    } catch (error) {
        next(error);
    }
});

app.get('/tasks/:showClosed', async (req, res, next) => {
    const showClosed = req.params.showClosed;

    try {
        const results = await tasksRepository.getTasksList(showClosed == '1' ? true : false);

        res.json(results);
    } catch (error) {
        next(error);
    }
});

app.post('/task/:key/close', async (req, res, next) => {
    const key = req.params.key;

    try {
        await tasksRepository.closeTask(key);
        res.status(200).send('');
    } catch (error) {
        next(error);
    }
});

app.post('/task/:key/report', async (req, res, next) => {
    const key = req.params.key;

    try {
        await worklogsRepository.closeWorklog();
        await tasksRepository.markTaskAsReported(key);

        res.status(200).send('');
    } catch (error) {
        next(error);
    }
});

app.get('/task/:key/unreported-worklogs/:date', async (req, res, next) => {
    const key = req.params.key;
    const date = req.params.date;

    try {
        const results = await tasksRepository.getTaskUnreportedWorklogs(key, date);

        res.json({results});
    } catch (error) {
        next(error);
    }
});

app.get('/task/:key/unreported-duration', async (req, res, next) => {
    const key = req.params.key;

    try {
        const duration = await tasksRepository.getTaskUnreportedDuration(key);
        res.json({duration});
    } catch (error) {
        next(error);
    }
});

app.get('/task/:key/unreported-duration/:date', async (req, res, next) => {
    const key = req.params.key;
    const date = req.params.date;

    try {
        const duration = await tasksRepository.getTaskUnreportedDuration(key, date);
        res.json({duration});
    } catch (error) {
        next(error);
    }
});

app.get('/task/:key/total-duration', async (req, res, next) => {
    const key = req.params.key;

    try {
        const duration = await tasksRepository.getTaskTotalDuration(key);

        res.json({duration});
    } catch (error) {
        next(error);
    }
});

app.get('/task/current-running/info', async (req, res, next) => {
    try {
        const runningTask = await tasksRepository.getCurrentRunningTask();

        if (null !== runningTask) {
            const result = await tasksRepository.findTaskByKey(runningTask.key);

            res.json(result);

            return;
        }

        res.status(404).json({result: 'not found'});
    } catch (error) {
        next(error);
    }
});


// ~

app.post('/worklog', async (req, res, next) => {
    const key = req.body.key;

    try {
        await worklogsRepository.closeWorklog();
        await worklogsRepository.createWorklog(key);

        res.status(201).send('');
    } catch (error) {
        next(error);
    }
});

app.post('/worklog/close', async (req, res, next) => {
    try {
        await worklogsRepository.closeWorklog();

        res.status(200).send('');
    } catch (error) {
        next(error);
    }
});

// ~

app.get('/time-distribution/:date', async (req, res, next) => {
    try {
        const date = req.params.date;
        const results = await worklogsRepository.getTimeDistribution(date);

        res.json(results);
    } catch (error) {
        next(error);
    }
});

app.get('/daily-history/:date', async (req, res, next) => {
    try {
        const date = req.params.date;
        const results = await worklogsRepository.getDailyHistory(date);

        res.json(results);
    } catch (error) {
        next(error);
    }
});

app.post('/worklogs/mark-as-reporting', async (req, res, next) => {
    try {
        const worklogIds = req.body.worklogIds;
        await worklogsRepository.markAsReporting(worklogIds);

        res.status(200).send('');
    } catch (error) {
        next(error);
    }
});

app.post('/worklogs/mark-as-reported', async (req, res, next) => {
    const worklogIds = req.body.worklogIds;

    try {
        await worklogsRepository.markAsReported(worklogIds);

        res.status(200).send('');
    } catch (error) {
        next(error);
    }
});

app.use((err, req, res, next) => {
    console.error(`ERROR: ${err}`);

    if (res.headersSent) {
        return next(err);
    }

    res.status(500);
    res.json({error: ''+err});
});

app.listen(config.database.port, async () => {
    console.log(`MySQL listening on port ${config.database.port}.`);
    const conn = await pool.getConnection();
    const now = await conn.query('SELECT @@GLOBAL.wait_timeout AS timeout');
    console.log(`MySQL wait timeout is ${now[0]['timeout']} seconds.`);
});