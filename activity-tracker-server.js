'use strict';

const ioHook = require('iohook');
const WebSocketServer = require('websocket').server;
const http = require('http');
const config = require('./src/config');

let reporterHandle = null;
let state = 'idle';
let connection = null;

const reporter = () => {
    if (null !== connection) {
        console.log('Reporting idleness...');
        connection.sendUTF(JSON.stringify({type: 'idle'}));

        state = 'idle';
    }
};

ioHook.on("mousemove", event => {
    clearTimeout(reporterHandle);
    reporterHandle = setTimeout(reporter, config.activityTracker.idleSecondsThreshold * 1000);

    if (state === 'idle' && null !== connection) {
        console.log('Reporting activity...');
        connection.sendUTF(JSON.stringify({type: 'active'}));

        state = 'active';
    }
});

ioHook.on("keydown", event => {
    clearTimeout(reporterHandle);
    reporterHandle = setTimeout(reporter, config.activityTracker.idleSecondsThreshold * 1000);

    if (state === 'idle' && null !== connection) {
        console.log('Reporting activity...');
        connection.sendUTF(JSON.stringify({type: 'active'}));

        state = 'active';
    }
});

ioHook.start();

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

const server = http.createServer(function(request, response) {
    console.log(`Received request for ${request.url}`);
    response.writeHead(404);
    response.end();
});

server.timeout = 0;

server.listen(config.activityTracker.port, function() {
    console.log(`Server is listening on port ${config.activityTracker.port}.`);
});

const wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

wsServer.on('request', function(request) {
    connection = request.accept('echo-protocol', request.origin);
    console.log('Connection accepted.');

    if (state === 'idle') {
        console.log('Reporting idleness...');
        connection.sendUTF(JSON.stringify({type: 'idle'}));
    }

    if (state === 'active') {
        console.log('Reporting activity...');
        connection.sendUTF(JSON.stringify({type: 'active'}));
    }

    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log(`Received message: ${message.utf8Data}`);
        }
    });

    connection.on('close', function(reasonCode, description) {
        console.log('Peer disconnected.');
        connection = null;
    });
});
