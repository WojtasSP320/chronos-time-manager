const localhost = '10.0.1.52';

module.exports = {
    chronos: {
        fullscreen: false,
        host: `http://${localhost}:5873`
    },
    jira: {
        host: 'http://10.0.0.41:8080',
        username: 'user',
        password: '********'
    },
    jiraProxy: {
        port: 3942,
        host: `http://${localhost}:3942`,
    },
    database: {
        port: 7621,
        host: 'localhost',
        user: 'user',
        password: '********',
        database: 'chronos',
        restHost: `http://${localhost}:7621`,
    },
    activityTracker: {
        port: 54138,
        host: `ws://${localhost}:54138`,
        idleSecondsThreshold: 300,
    }
};