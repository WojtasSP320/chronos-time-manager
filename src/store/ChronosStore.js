module.exports = {
    state: {
        isUserActive: null,
        currentTask: null,
        isUserWorkingAwayFromKeyboard: false,
        lastCodingTask: null,
        permanentTasks: {},
        showClosedLocalTasks: false,
        lockedTask: null,
        today: null,
        jiraBoard: null,
        activeSprints: {},
    },
    getters: {
        isUserActive: (state) => {
            return state.isUserActive;
        },
        getCurrentTask: (state) => {
            return state.currentTask;
        },
        isUserWorkingAwayFromKeyboard: (state) => {
            return state.isUserWorkingAwayFromKeyboard;
        },
        getLastCodingTask: (state) => {
            return state.lastCodingTask;
        },
        getPermanentTasks: (state) => {
            return state.permanentTasks;
        },
        getShowClosedLocalTasks: (state) => {
            return state.showClosedLocalTasks;
        },
        getLockedTask: (state) => {
            return state.lockedTask;
        },
        getToday: (state) => {
            return state.today;
        },
        getJiraBoard: (state) => {
            return state.jiraBoard;
        },
        getActiveSprints: (state) => {
            return state.activeSprints;
        }
    },
    mutations: {
        setUserActive(state, isUserActive) {
            state.isUserActive = isUserActive;
        },
        setCurrentTask(state, currentTask) {
            state.currentTask = currentTask;
        },
        setUserWorkingAwayFromKeyboard(state, isUserWorkingAwayFromKeyboard) {
            state.isUserWorkingAwayFromKeyboard = isUserWorkingAwayFromKeyboard;
        },
        setLastCodingTask(state, lastCodingTask) {
            state.lastCodingTask = lastCodingTask;
        },
        setPermanentTasks(state, permanentTasks) {
            state.permanentTasks = permanentTasks;
        },
        setPermanentTaskByKey(state, {key, task}) {
            state.permanentTasks[key] = task;
        },
        setShowClosedLocalTasks(state, showClosedLocalTasks) {
            state.showClosedLocalTasks = showClosedLocalTasks;
        },
        setLockedTask(state, lockedTask) {
            state.lockedTask = lockedTask;
        },
        setToday(state, today) {
            state.today = today;
        },
        setJiraBoard(state, boardId) {
            state.jiraBoard = boardId;
        },
        setActiveSprints(state, sprints) {
            state.activeSprints = sprints;
        },
    },
    actions: {},
};