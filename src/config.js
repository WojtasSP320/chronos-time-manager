const localConfig = require('../config.local.js');

module.exports = {
    chronos: {
        fullscreen: localConfig.chronos.fullscreen,
        host: localConfig.chronos.host
    },
    jira: {
        host: localConfig.jira.host,
        username: localConfig.jira.username,
        password: localConfig.jira.password
    },
    jiraProxy: {
        port: localConfig.jiraProxy.port,
        host: localConfig.jiraProxy.host,
    },
    database: {
        port: localConfig.database.port,
        host: localConfig.database.host,
        user: localConfig.database.user,
        password: localConfig.database.password,
        database: localConfig.database.database,
        restHost: localConfig.database.restHost,
    },
    activityTracker: {
        port: localConfig.activityTracker.port,
        host: localConfig.activityTracker.host,
        idleSecondsThreshold: localConfig.activityTracker.idleSecondsThreshold,
    }
};