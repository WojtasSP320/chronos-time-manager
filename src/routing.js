import Dashboard from "./pages/Dashboard";
import LocalTasksList from "./pages/LocalTasksList";
import RemoteTasksList from "./pages/RemoteTasksList";
import TaskRun from "./pages/TaskRun";
import TodaysTimeDistribution from "./pages/TodaysTimeDistribution";

export default [
    { path: '/', name: 'dashboard', component: Dashboard},
    { path: '/tasks', name: 'local_tasks_list', component: LocalTasksList},
    { path: '/import-task', name: 'remote_tasks_list', component: RemoteTasksList},
    { path: '/task/:key', name: 'task_run',  component: TaskRun},
    { path: '/todays-time-distribution', name: 'todays_time_distribution',  component: TodaysTimeDistribution},
    { path: '*', redirect: {name: 'dashboard'}},
]