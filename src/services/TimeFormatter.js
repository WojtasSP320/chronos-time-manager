module.exports = class TimeFormatter {
    /**
     * @param {Number} duration
     * @param {Boolean} fancy
     *
     * @returns {string}
     */
    static format(duration, fancy = false) {
        const seconds = duration % 60;
        const minutes = Math.floor(duration / 60) % 60;
        const hours = Math.floor(duration / 3600) % 8;
        const days = Math.floor(duration / 3600 / 8);

        switch (true) {
            case days > 0:
                return `${days}d ${hours}h ${minutes}m ${seconds}s`;

            case hours > 0:
                return `${hours}h ${minutes}m ${seconds}s`;

            case minutes > 0:
                return `${minutes}m ${seconds}s`;

            default:
                return true === fancy
                    ? `Started ${seconds}s ago`
                    : `${seconds}s`
                ;
        }
    }
};