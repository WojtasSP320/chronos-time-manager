import DbClient from "../clients/DbClient";
import JiraClient from "../clients/JiraProxyClient";

export default class WorklogManager {
    constructor() {
        this.database = new DbClient();
        this.jira = new JiraClient();
    }

    /**
     * @param {string} key
     * @returns {Promise<void>}
     */
    async openNewWorklog(key) {
        await this.database.openNewWorklog(key);
    }

    /**
     * @returns {Promise<void>}
     */
    async closeWorklog() {
        await this.database.closeWorklog();
    }

    /**
     * @param {string} key
     * @returns {Promise<void>}
     */
    async reportTaskWorklogs(key) {
        const duration = await this.getUnreportedDuration(key);

        if (null === duration) {
            throw new Error(`Cannot report "null" time for task ${key}!`);
        }

        await this.jira.reportWorklog(key, duration);
        await this.database.reportTaskWorklogs(key);
    }


    /**
     * @param {string} key
     * @returns {Promise<void>}
     */
    async closeWorklogAndReport(key) {
        await this.database.closeWorklog();
        await this.reportTaskWorklogs(key);
    }

    /**
     * @param {string} key
     * @returns {Promise<*>}
     */
    async getUnreportedDuration(key) {
        const {data} = await this.database.getTaskUnreportedDuration(key);

        return data.duration;
    }

    /**
     * @param {string} key
     * @param {string} date
     *
     * @returns {Promise<*>}
     */
    async getUnreportedDurationForDay(key, date) {
        const {data} = await this.database.getTaskUnreportedDurationForDay(key, date);

        return data.duration;
    }

    /**
     * @param {string} key
     * @param {string} date
     *
     * @returns {Promise<*>}
     */
    async getUnreportedWorklogs(key, date) {
        const {data} = await this.database.getTaskUnreportedWorklogs(key, date);

        return data.results;
    }

    /**
     * @param {array} worklogs
     *
     * @returns {Promise<T>}
     */
    async markWorklogsAsReporting(worklogs) {
        const {data} = await this.database.markWorklogsAsReporting(worklogs);

        return data;
    }

    /**
     * @param {array} worklogs
     *
     * @returns {Promise<T>}
     */
    async markWorklogsAsReported(worklogs) {
        const {data} = await this.database.markWorklogsAsReported(worklogs);

        return data;
    }
}