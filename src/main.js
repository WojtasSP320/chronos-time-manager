import Vue from 'vue';
import App from './App';
import VueRouter from 'vue-router';
import VueToastr2 from 'vue-toastr-2';
import Vuex from 'vuex';
import 'vue-toastr-2/dist/vue-toastr-2.min.css';
import chronosStore from './store/ChronosStore';

window.toastr = require('toastr');

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(Vuex);

import Vue2TouchEvents from 'vue2-touch-events';
import Calendar from 'v-calendar/lib/components/calendar.umd'

const store = new Vuex.Store(chronosStore);

Vue.component('calendar', Calendar);

Vue.use(Vue2TouchEvents);

Vue.use(VueToastr2, {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-full-width",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "3000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
});

new Vue({
    store,
    render: h => h(App),
}).$mount('#app');
