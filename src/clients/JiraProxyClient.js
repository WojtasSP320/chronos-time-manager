import axios from 'axios';
import config from '../config';

const JIRA_REST_BASE_URL = `${config.jiraProxy.host}`;

export default class JiraProxyClient {
    constructor() {
        this.axios = axios.create();
    }

    /**
     * @returns {Promise<*>}
     */
    async getMyOpenIssuesList() {
        const {data} = await this.axios.post(`${JIRA_REST_BASE_URL}/get-my-open-issues-list`);

        return data.issues;
    }

    /**
     * @param {string} jql
     *
     * @returns {Promise<*>}
     */
    async findIssue(jql) {
        const {data} = await this.axios.post(`${JIRA_REST_BASE_URL}/find-issue`, {jql});

        return data.issues;
    }

    /**
     * @param {string} project
     * @param {string} summary
     * @param {string} description
     * @param {string} issueType
     * @param {string} dueDate
     * @param {Number|null} sprintId
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async createIssue(project, summary, description, issueType, dueDate, sprintId) {
        const data = {
            fields: {
                project: {
                    key: project
                },
                summary: summary,
                description: description,
                issuetype: {
                    name: issueType
                },
                duedate: dueDate,
                assignee: {
                    name: config.jira.username
                },
                customfield_10004: sprintId,
            }
        };

        return await this.axios.post(`${JIRA_REST_BASE_URL}/issue`, data);
    }

    /**
     * @param {string} issueKey
     * @param {Number} timeSpentSeconds
     * @param {string} startDate
     * @param {string} comment
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async reportWorklog(issueKey, timeSpentSeconds, startDate, comment) {
        return await this.axios.post(`${JIRA_REST_BASE_URL}/report-worklog/${issueKey}`, {
            timeSpentSeconds,
            startDate,
            comment
        });
    }

    /**
     * @param {Number} boardId
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getActiveSprint(boardId) {
        const {data} = await this.axios.get(`${JIRA_REST_BASE_URL}/active-sprint/${boardId}`);

        return data.values[0] || null;
    }
}