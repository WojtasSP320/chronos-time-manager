const axios = require('axios');
const config = require('../config');
const moment = require('moment');

const JIRA_REST_BASE_URL = `${config.jira.host}`;
const MINIMUM_TIME_SPENT_SECONDS = 60;

module.exports = class JiraClient {
    constructor() {
        this.axios = axios.create({
            auth: {
                username: config.jira.username,
                password: config.jira.password,
            }
        });
    }

    /**
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getMyOpenIssuesList() {
        return await this.axios.post(`${JIRA_REST_BASE_URL}/rest/api/2/search`, {
            jql: 'status in (Backlog, "In progress", "In Review", "To Do", Oczekuje, Open, Otwarty, "Pull request") AND assignee in (currentUser())',
        });
    }

    /**
     * @param {string} jql
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async findIssue(jql) {
        return await this.axios.post(`${JIRA_REST_BASE_URL}/rest/api/2/search`, {jql});
    }

    /**
     * @param {string} data
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async createIssue(data) {
        return await this.axios.post(`${JIRA_REST_BASE_URL}/rest/api/2/issue`, data);
    }

    /**
     * @param {string} issueKey
     * @param {Number} timeSpentSeconds
     * @param {string} startDate
     * @param {string} comment
     * @returns {Promise<AxiosResponse<T>>}
     */
    async reportWorklog(issueKey, timeSpentSeconds, startDate, comment) {
        comment = comment || 'Time reported with Chronos';
        startDate = startDate || moment().format('YYYY-MM-DDTHH:mm:ss.SSSZZ');
        timeSpentSeconds = Math.max(timeSpentSeconds, MINIMUM_TIME_SPENT_SECONDS);

        return await this.axios.post(`${JIRA_REST_BASE_URL}/rest/api/2/issue/${issueKey}/worklog`, {
            timeSpentSeconds,
            started: startDate,
            comment,
        });
    }

    /**
     * @param {Number} boardId
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getActiveSprint(boardId) {
        return await this.axios.get(`${JIRA_REST_BASE_URL}/rest/agile/1.0/board/${boardId}/sprint?state=active`);
    }
};