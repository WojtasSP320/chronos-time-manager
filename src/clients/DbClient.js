import axios from 'axios';
import config from '../config';
import moment from "moment";

const DB_REST_BASE_URL = config.database.restHost;

export default class DbClient {
    constructor() {
        this.axios = axios.create();
    }

    /**
     * @param {string} key
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getTask(key) {
        const {data} = await this.axios.get(`${DB_REST_BASE_URL}/task/${key}`);

        return data;
    }

    /**
     * @param {string} type
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getTasksByType(type) {
        const {data} =  await this.axios.get(`${DB_REST_BASE_URL}/tasks/by-type/${type}`);

        return data.reduce((tasks, task) => {
            task.attributes = JSON.parse(task.attributes);
            return {...tasks, [task.key]: task};
        }, {});
    }

    /**
     * @param {Number} showClosed
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getTasksList(showClosed) {
        const {data} = await this.axios.get(`${DB_REST_BASE_URL}/tasks/${showClosed}`);

        return data;
    }

    /**
     * @param {string} key
     * @param {string} summary
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async createTask(key, summary) {
        return await this.axios.post(`${DB_REST_BASE_URL}/task`, {key, summary});
    }

    /**
     * @param {string} key
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async closeTask(key) {
        return await this.axios.post(`${DB_REST_BASE_URL}/task/${key}/close`);
    }

    /**
     * @param {string} key
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async reportTaskWorklogs(key) {
        return await this.axios.post(`${DB_REST_BASE_URL}/task/${key}/report`, {key});
    }

    /**
     * @param {string} key
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getTaskUnreportedDuration(key) {
        return await this.axios.get(`${DB_REST_BASE_URL}/task/${key}/unreported-duration`);
    }

    /**
     * @param {string} key
     * @param {string} date
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getTaskUnreportedDurationForDay(key, date) {
        return await this.axios.get(`${DB_REST_BASE_URL}/task/${key}/unreported-duration/${date}`);
    }

    /**
     * @param {string} key
     * @param {string} date
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getTaskUnreportedWorklogs(key, date) {
        return await this.axios.get(`${DB_REST_BASE_URL}/task/${key}/unreported-worklogs/${date}`);
    }

    /**
     * @param {string} key
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getTaskTotalDuration(key) {
        return await this.axios.get(`${DB_REST_BASE_URL}/task/${key}/total-duration`);
    }

    /**
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getCurrentRunningTask() {
        const {data} = await this.axios.get(`${DB_REST_BASE_URL}/task/current-running/info`);

        return data;
    }

    /**
     * @param {string} key
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async openNewWorklog(key) {
        return await this.axios.post(`${DB_REST_BASE_URL}/worklog`, {key});
    }

    /**
     * @returns {Promise<AxiosResponse<T>>}
     */
    async closeWorklog() {
        return await this.axios.post(`${DB_REST_BASE_URL}/worklog/close`, {});
    }

    /**
     * @param {string} date
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getTimeDistribution(date) {
        return await this.axios.get(`${DB_REST_BASE_URL}/time-distribution/${date}`);
    }

    /**
     * @returns {Promise<AxiosResponse<T>>}
     */
    async getDailyHistory(date = null) {
        if (null === date) {
            date = moment().format('YYYY-MM-DD');
        }

        const {data} = await this.axios.get(`${DB_REST_BASE_URL}/daily-history/${date}`);

        return data;
    }

    /**
     * @param {array} worklogs
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async markWorklogsAsReporting(worklogs) {
        const worklogIds = worklogs.map((worklog) => {return worklog.id;});

        return await this.axios.post(`${DB_REST_BASE_URL}/worklogs/mark-as-reporting`, {worklogIds});
    }

    /**
     * @param {array} worklogs
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    async markWorklogsAsReported(worklogs) {
        const worklogIds = worklogs.map((worklog) => {return worklog.id;});

        return await this.axios.post(`${DB_REST_BASE_URL}/worklogs/mark-as-reported`, {worklogIds});
    }
}