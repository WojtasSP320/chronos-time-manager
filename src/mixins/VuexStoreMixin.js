const {mapGetters, mapMutations} = require("vuex");

module.exports = {
    computed: {
        ...mapGetters([
            'isUserActive',
            'getCurrentTask',
            'isUserWorkingAwayFromKeyboard',
            'getLastCodingTask',
            'getPermanentTasks',
            'getShowClosedLocalTasks',
            'getLockedTask',
            'getToday',
            'getJiraBoard',
            'getActiveSprints',
        ]),
    },
    methods: {
        ...mapMutations([
            'setUserActive',
            'setCurrentTask',
            'setUserWorkingAwayFromKeyboard',
            'setLastCodingTask',
            'setPermanentTasks',
            'setPermanentTaskByKey',
            'setShowClosedLocalTasks',
            'setLockedTask',
            'setToday',
            'setJiraBoard',
            'setActiveSprints',
        ]),
    }
};