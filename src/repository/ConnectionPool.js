const mysql = require("promise-mysql");
const config = require('../config');

class ConnectionPool {
    constructor() {
        this.pool = null;
    }

    async _getPool() {
        if (this.pool === null) {
            this.pool = await mysql.createPool({
                connectionLimit: 1,
                host     : config.database.host,
                user     : config.database.user,
                password : config.database.password,
                database : config.database.database
            });
        }

        return this.pool;
    }

    async getConnection() {
        const pool = await this._getPool();
        return pool;
    }
}

const connectionPool = new ConnectionPool();

module.exports = connectionPool;