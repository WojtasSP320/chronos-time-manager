const RepositoryAbstract = require('./RepositoryAbstract');

class WorklogsRepository extends RepositoryAbstract {

    /**
     * @param {string} key
     * @param {string} date
     *
     * @returns {Promise<*>}
     */
    async getUnreportedWorklogs(key, date) {
        const sql = `
            SELECT 
                   id,
                   task, 
                   started_date, 
                   IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at AS duration 
            FROM 
                 worklogs 
            WHERE 
                  task = ? AND 
                  reported = 0
        `;

        const parameters = [
            key,
            date,
        ];

        return await this.query(sql, parameters);
    }

    /**
     * @param {string} key
     *
     * @returns {Promise<*>}
     */
    async createWorklog(key) {
        const sql = `
            INSERT INTO 
                worklogs 
                (
                    started_at, 
                    started_date, 
                    task
                ) 
            VALUES 
                (
                    UNIX_TIMESTAMP(), 
                    NOW(), 
                    ?
                )
        `;

        const parameters = [
            key
        ];

        await this.query(sql, parameters);
    }

    /**
     * @returns {Promise<void>}
     */
    async closeWorklog() {
        const sql = `
            UPDATE 
                worklogs 
            SET 
                ended_at = UNIX_TIMESTAMP(), 
                ended_date = NOW(), 
                duration =  UNIX_TIMESTAMP() - started_at 
            WHERE 
                  ended_at IS NULL 
            LIMIT 1
        `;

        const parameters = [];

        await this.query(sql, parameters);
    }

    /**
     * @param {string} date
     *
     * @returns {Promise<*>}
     */
    async getTimeDistribution(date) {
        const sql = `
            SELECT 
                task, 
                SUM(IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at) AS duration 
            FROM 
                 worklogs
            WHERE 
                  YEAR(started_date) = YEAR(?) AND 
                  MONTH(started_date) = MONTH(?) AND 
                  DAYOFMONTH(started_date) = DAYOFMONTH(?)
            GROUP BY 
                 task
        `;

        const parameters = [
            date,
            date,
            date,
        ];

        return await this.query(sql, parameters);
    }

    async getDailyHistory(date) {
        const sql = `
            SELECT 
                * 
            FROM 
                worklogs 
            WHERE 
                YEAR(started_date) = YEAR(?) AND 
                MONTH(started_date) = MONTH(?) AND
                DAYOFMONTH(started_date) = DAYOFMONTH(?)
        `;

        const parameters = [
            date,
            date,
            date,
        ];

        return await this.query(sql, parameters);
    }

    /**
     * @param {array} worklogIds
     *
     * @returns {Promise<void>}
     */
    async markAsReporting(worklogIds) {
        worklogIds.push(0);
        const worklogs = worklogIds.join(',');

        const sql = `
            UPDATE 
                worklogs 
            SET 
                reported = 2
            WHERE 
                id IN (${worklogs}) 
        `;

        const parameters = [];

        await this.query(sql, parameters);
    }

    /**
     * @param {array} worklogIds
     *
     * @returns {Promise<void>}
     */
    async markAsReported(worklogIds) {
        worklogIds.push(0)
        const worklogs = worklogIds.join(',');

        const sql = `
            UPDATE 
                worklogs 
            SET 
                reported = 1
            WHERE 
                id IN (${worklogs}) AND
                reported = 2
        `;

        const parameters = [];

        await this.query(sql, parameters);
    }
}

module.exports = new WorklogsRepository();