const connectionPool = require("./ConnectionPool");

module.exports = class RepositoryAbstract {
     /**
     * @param {string} sql
     * @param {array} parameters
     *
     * @returns {Promise<*>}
     */
    async query(sql, parameters) {
        const connection = await connectionPool.getConnection();

        return await connection.query(sql, parameters);
    }
};