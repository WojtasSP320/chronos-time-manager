const RepositoryAbstract = require('./RepositoryAbstract');

class TasksRepository extends RepositoryAbstract {

    /**
     * @param {string} key
     *
     * @return {Promise<*>}
     */
    async findTaskByKey(key) {
        const sql = `
            SELECT 
                   tasks.*, 
                   SUM(IF(reported = 0, IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at, 0)) AS unreported_duration, 
                   SUM(IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at) AS duration 
            FROM 
                 tasks 
            LEFT JOIN 
                 worklogs ON tasks.\`key\` = worklogs.task 
            WHERE \`key\` = ? 
            GROUP BY \`key\` 
            LIMIT 1
        `;

        const parameters = [
            key,
        ];

        return await this.query(sql, parameters);
    }

    /**
     * @param {boolean} includeClosed
     *
     * @returns {Promise<*>}
     */
    async getTasksList(includeClosed) {
        let where = `tasks.closed = 0 AND type = 'task'`;

        if (includeClosed) {
            where = `tasks.closed IN (0,1) AND type = 'task'`;
        }

        const sql = `
            SELECT 
                tasks.*, 
                SUM(IF(reported = 0, IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at, 0)) AS unreported_duration, 
                SUM(IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at) AS duration 
            FROM 
                tasks 
            LEFT JOIN 
                worklogs on tasks.\`key\` = worklogs.task 
            WHERE 
                ${where} 
            GROUP BY \`key\` 
            ORDER BY 
                closed ASC, \`key\` ASC
        `;

        return await this.query(sql);
    }

    /**
     * @param {string} type
     *
     * @returns {Promise<*>}
     */
    async getTasksByType(type) {
        const sql = `
            SELECT 
                   tasks.*, 
                   SUM(IF(reported = 0, IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at, 0)) AS unreported_duration, 
                   SUM(IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at) AS duration 
            FROM 
                 tasks 
            LEFT JOIN 
                 worklogs ON tasks.\`key\` = worklogs.task 
            WHERE 
                  type = ? 
            GROUP BY 
                 \`key\`
        `;

        const parameters = [
            type,
        ];

        return await this.query(sql, parameters);
    }


    /**
     * @returns {Promise<*>}
     */
    async getCurrentRunningTask() {
        const sql = `
            SELECT task AS \`key\`,
                   summary,
                   type
            FROM worklogs
                     JOIN
                 tasks t on worklogs.task = t.\`key\`
            WHERE ended_at IS NULL
            LIMIT 1
        `;

        const parameters = [];

        const results = await this.query(sql, parameters);

        if (results.length > 0) {
            return results[0];
        }

        return null;
    }

    /**
     * @param {string} key
     *
     * @returns {Promise<*>}
     */
    async getTaskTotalDuration(key) {
        const sql = `
            SELECT 
                SUM(IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at) AS duration 
            FROM 
                 worklogs 
            WHERE task = ?
        `;

        const parameters = [
            key
        ];

        const results = await this.query(sql, parameters);

        if (results.length > 0) {
            return results[0].duration;
        }

        return 0;
    }

    /**
     * @param {string} key
     * @param {string|null} date
     *
     * @returns {Promise<*>}
     */
    async getTaskUnreportedDuration(key, date = null) {
        const dateCriterion = null !== date
            ? `AND DATE(started_date) = ${date}`
            : ''
        ;

        const sql = `
            SELECT 
                   SUM(IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at) AS duration 
            FROM 
                 worklogs 
            WHERE 
                  task = ? AND 
                  reported = 0
                  ${dateCriterion}
        `;

        const parameters = [
            key
        ];

        const results = await this.query(sql, parameters);

        if (results.length > 0) {
            return results[0].duration;
        }

        return 0;
    }

    /**
     * @param {string} key
     * @param {string} date
     *
     * @returns {Promise<*>}
     */
    async getTaskUnreportedWorklogs(key, date) {
        const sql = `
            SELECT 
                id,
                task, 
                started_date, 
                IFNULL(ended_at, UNIX_TIMESTAMP()) - started_at AS duration 
            FROM 
                 worklogs 
            WHERE 
                  task = ? AND
                  DATE(started_date) = ? AND
                  reported = 0
        `;

        const parameters = [
            key,
            date
        ];

        return await this.query(sql, parameters);
    }

    /**
     * @param {string} key
     *
     * @returns {Promise<*>}
     */
    async markTaskAsReported(key) {
        const sql = `
            UPDATE 
                worklogs 
            SET 
                reported = 1 
            WHERE 
              task = ? AND 
              reported = 0
        `;

        const parameters = [
            key
        ];

        return await this.query(sql, parameters);
    }

    /**
     * @param {string} key
     *
     * @returns {Promise<*>}
     */
    async closeTask(key) {
        const sql = `
            UPDATE 
                tasks 
            SET 
                closed = 1 
            WHERE 
                  \`key\` = ? 
            LIMIT 1
        `;

        const parameters = [
            key
        ];

        return await this.query(sql, parameters);
    }

    /**
     * @param {string} key
     * @param {string} summary
     *
     * @returns {Promise<*>}
     */
    async createTask(key, summary) {
        const sql = `
            INSERT INTO 
                tasks 
                (
                    \`key\`, 
                    closed, 
                    summary
                ) 
            VALUES 
                (
                    ?, 
                    0, 
                    ?
                )
        `;

        const parameters = [
            key,
            summary,
        ];

        return await this.query(sql, parameters);
    }
}

module.exports = new TasksRepository();