/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `key` varchar(255) NOT NULL,
  `closed` tinyint(4) NOT NULL DEFAULT 0,
  `summary` mediumtext DEFAULT NULL,
  `type` varchar(128) NOT NULL DEFAULT 'task',
  `search_jql` longtext DEFAULT NULL,
  `attributes` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `icon` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `tasks_key_uindex` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES
('BRAINSTORM',0,'Trying to figure something out','permanent','assignee = currentUser() AND type = \"Brainstorm\"  AND status in (\"To Do\", \"In progress\") AND dueDate >= {{startOfDay}} AND dueDate  <= {{endOfDay}} ','{\"project\": \"COMPANY\", \"summary\": \"Brainstorm\", \"description\": \"Brainstrom\", \"type\":\"Brainstorm\"}','fa fa-lightbulb-o'),
('DEPLOYMENT',0,'Pushing code to production','permanent','assignee = currentUser() AND type = \"Deploy\"  AND status in (\"To Do\", \"In progress\") AND dueDate >= {{startOfDay}} AND dueDate  <= {{endOfDay}} ','{\"project\": \"COMPANY\", \"summary\": \"Deploy\", \"description\": \"Deploy\", \"type\":\"Deploy\"}','fa fa-database'),
('HARDWARE',0,'Technical support, hardware, etc.','permanent','assignee = currentUser() AND type = \"Technical support\"  AND status in (\"To Do\", \"In progress\") AND dueDate >= {{startOfDay}} AND dueDate  <= {{endOfDay}} ','{\"project\": \"COMPANY\", \"summary\": \"Technical support\", \"description\": \"Technical support\", \"type\":\"Technical support\"}','fa fa-wrench'),
('HELP',0,'Helping colleagues','permanent','assignee = currentUser() AND type = \"Friendly help\"  AND status in (\"To Do\", \"In progress\") AND dueDate >= {{startOfDay}} AND dueDate  <= {{endOfDay}} ','{\"project\": \"COMPANY\", \"summary\": \"Friendly help\", \"description\": \"Friendly help\", \"type\":\"Friendly help\"}','fa fa-life-ring'),
('MEETINGS',0,'Seeing other people (blah!)','permanent','assignee = currentUser() AND type = \"Meeting\"  AND status in (\"To Do\", \"In progress\") AND dueDate >= {{startOfDay}} AND dueDate  <= {{endOfDay}} ','{\"project\": \"COMPANY\", \"summary\": \"Meeting\", \"description\": \"Meeting\", \"type\":\"Meeting\"}','fa fa-users'),
('OTHER',0,'Doing some stuff','permanent','assignee = currentUser() AND type = \"Other\" AND status in (\"To Do\", \"In progress\") AND dueDate >= {{startOfDay}} AND dueDate  <= {{endOfDay}} ','{\"project\": \"COMPANY\", \"summary\": \"Other\", \"description\": \"Other\", \"type\":\"Other\"}','fa fa-user-secret'),
('REVIEW',0,'Code review','permanent','assignee = currentUser() AND type = \"Code Review\"  AND status in (\"To Do\", \"In progress\") AND dueDate >= {{startOfDay}} AND dueDate  <= {{endOfDay}} ','{\"project\": \"COMPANY\", \"summary\": \"Code Review\", \"description\": \"Code Review\", \"type\":\"Code Review\"}','fa fa-binoculars'),
('SERVICE',0,'Handling service desk issues','permanent','assignee = currentUser() AND type = \"Service Desk\"  AND status in (\"To Do\", \"In progress\") AND dueDate >= {{startOfDay}} AND dueDate  <= {{endOfDay}} ','{\"project\": \"COMPANY\", \"summary\": \"Service Desk\", \"description\": \"Service Desk\", \"type\":\"Service Desk\"}','fa fa-bug')
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worklogs`
--

DROP TABLE IF EXISTS `worklogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worklogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `started_at` int(11) NOT NULL,
  `started_date` datetime NOT NULL,
  `ended_at` int(11) DEFAULT NULL,
  `ended_date` datetime DEFAULT NULL,
  `task` varchar(255) NOT NULL,
  `reported` tinyint(4) NOT NULL DEFAULT 0,
  `duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `worklogs_tasks_key_fk` (`task`),
  CONSTRAINT `worklogs_tasks_key_fk` FOREIGN KEY (`task`) REFERENCES `tasks` (`key`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=496 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worklogs`
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
