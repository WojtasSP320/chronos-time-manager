let { spawn } = require('child_process');

const ERROR_BEGIN = "\x1b[31m";
const ERROR_END = "\x1b[0m";

function now() {
    return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
}

console.log('Launching database server...');
let databaseServer = spawn('node', [`${__dirname}/database-server.js`]);

databaseServer.stdout.on('data', (data) => {
    process.stdout.write(`${now()} [DATABASE] ${data}`);
});

databaseServer.stderr.on('data', (data) => {
    process.stdout.write(`${ERROR_BEGIN}${now()} [DATABASE] ${data}${ERROR_END}`);
});

databaseServer.on('close', (code) => {
    process.stdout.write(`${now()} [DATABASE] Process exited with code ${code}`);
});

// ~

console.log('Launching activity tracker server...');
let activityTrackerServer = spawn('node', [`${__dirname}/activity-tracker-server.js`]);


activityTrackerServer.stdout.on('data', (data) => {
    process.stdout.write(`${now()} [TRACKER]  ${data}`);
});

activityTrackerServer.stderr.on('data', (data) => {
    process.stdout.write(`${ERROR_BEGIN}${now()} [TRACKER]  ${data}${ERROR_END}`);
});

activityTrackerServer.on('close', (code) => {
    process.stdout.write(`[TRACKER]  Process exited with code ${code}`);
});

// ~

console.log('Launching jira proxy server...');
let jiraProxyServer = spawn('node', [`${__dirname}/jira-proxy-server.js`]);


jiraProxyServer.stdout.on('data', (data) => {
    process.stdout.write(`${now()} [JIRA PRX] ${data}`);
});

jiraProxyServer.stderr.on('data', (data) => {
    process.stdout.write(`${ERROR_BEGIN}${now()} [JIRA PRX] ${data}${ERROR_END}`);
});

jiraProxyServer.on('close', (code) => {
    process.stdout.write(`${now()} [JIRA PRX] Process exited with code ${code}`);
});